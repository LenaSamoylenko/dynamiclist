﻿using a3.Models;
using System.Collections.Generic;

namespace ap3.Models
{
    public class Repository
    {
        public static List<City> FetchCities()
        {
            List<City> cities = new List<City>();
            cities.Add(new City() { ID = 1, Name = "Faridabaad" });
            cities.Add(new City() { ID = 2, Name = "Greater Noida" });
            return cities;
        }
        public static List<Locality> FetchLocalities()
        {
            List<Locality> localities = new List<Locality>();
            localities.Add(new Locality() { ID = 1, CityID = 1, Name = "East Faridabaad" });
            localities.Add(new Locality() { ID = 2, CityID = 1, Name = "West Faridabaad" });
            localities.Add(new Locality() { ID = 3, CityID = 2, Name = "East Noida" });
            localities.Add(new Locality() { ID = 4, CityID = 2, Name = "West Noida" });
            return localities;
        }
        public static List<SubLocality> FetchSubLocalities()
        {
            List<SubLocality> subLocalities = new List<SubLocality>();
            subLocalities.Add(new SubLocality() { ID = 1, LocalityID = 1, Name = "East Faridabaad Region 1" });
            subLocalities.Add(new SubLocality() { ID = 2, LocalityID = 1, Name = "East Faridabaad Region 2" });
            subLocalities.Add(new SubLocality() { ID = 3, LocalityID = 2, Name = "West Faridabaad Region 1" });
            subLocalities.Add(new SubLocality() { ID = 4, LocalityID = 2, Name = "West Faridabaad Region 2" });
            subLocalities.Add(new SubLocality() { ID = 5, LocalityID = 3, Name = "East Noida Region 1" });
            subLocalities.Add(new SubLocality() { ID = 6, LocalityID = 3, Name = "East Noida Region 2" });
            subLocalities.Add(new SubLocality() { ID = 7, LocalityID = 4, Name = "West Noida Region 1" });
            subLocalities.Add(new SubLocality() { ID = 8, LocalityID = 4, Name = "West Noida Region 2" });
            return subLocalities;
        }
    }
}
