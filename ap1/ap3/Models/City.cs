﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace ap3.Models
{
    public class City
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
