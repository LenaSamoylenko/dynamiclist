﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ap1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ap1.Controllers
{
    //http://asptipsandtricks.blogspot.com/2016/11/Cascading-dropdown-in-MVC.html
    public class CascadingDropdownController : Controller
    {
        public ActionResult CasDropDown()
        {
            List<SelectListItem> lst =
                new List<SelectListItem>()
            {
                new SelectListItem { Value="1",Text="Province 1" },
                new SelectListItem{Value="2",Text="Province 2"},
                new SelectListItem{Value="3",Text="Province 3"},
                new SelectListItem{Value="4",Text="Provice 4"}
            };

            ViewBag.Province = lst;

            return View();
        }

        [HttpPost]
        public JsonResult getDistricts(int provinceid)
        {
            List<District> dist = new List<District>();
            District[] DistArrary = { new District { DistrictID = 1, DistrictName = "District 1", ProvinceID = 1 },
                                     new District { DistrictID=3, DistrictName="District 2",ProvinceID=1 },
                                     new District { DistrictID=4, DistrictName="District 3",ProvinceID=2 },
                                     new District { DistrictID=5, DistrictName="District 4",ProvinceID=3 },
                                     new District { DistrictID=6, DistrictName="District 5",ProvinceID=3 },
                                     new District { DistrictID=7, DistrictName="District 6",ProvinceID=3 },
                                     new District { DistrictID=8, DistrictName="District 7",ProvinceID=4 },
                                     new District { DistrictID=9, DistrictName="District 8",ProvinceID=4 },
                                     new District { DistrictID=10, DistrictName="District 9",ProvinceID=4 },
                                     new District { DistrictID=2, DistrictName="District 10",ProvinceID=2 },
                                     new District { DistrictID=11, DistrictName="District 11",ProvinceID=1 }};

            District[] selDists = DistArrary.Where(p => p.ProvinceID == provinceid).ToArray();

            return Json(selDists);
        }
    }
}