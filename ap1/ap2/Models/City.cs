﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace ap5.Models
{
    public class City
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
