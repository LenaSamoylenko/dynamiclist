﻿namespace ap2.Models
{
    public class SubLocality
    {
        public int ID { get; set; }
        public int LocalityID { get; set; }
        public string Name { get; set; }
    }
}
