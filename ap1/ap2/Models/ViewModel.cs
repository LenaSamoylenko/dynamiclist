﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace ap2.Models
{
    public class ViewModel
    {
        [Required(ErrorMessage = "Please select a city")]
        [Display(Name = "City")]
        public int? SelectedCity { get; set; }
        [Required(ErrorMessage = "Please select a locality")]
        [Display(Name = "Locality")]
        public int? SelectedLocality { get; set; }
        [Required(ErrorMessage = "Please select a sub locality")]
        [Display(Name = "Sub Locality")]
        public int? SelectedSubLocality { get; set; }
        public SelectList CityList { get; set; }
        public SelectList LocalityList { get; set; }
        public SelectList SubLocalityList { get; set; }
    }
}
