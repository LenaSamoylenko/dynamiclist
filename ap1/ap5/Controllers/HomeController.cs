﻿using ap5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ap5.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewModel model = new ViewModel();
            ConfigureViewModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(ViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ConfigureViewModel(model);
                return View(model);
            }
            // save and redirect
            return RedirectToAction("Somewhere");
        }

        [HttpGet]
        public JsonResult FetchLocalities(int ID)
        {
            var data = Repository.FetchLocalities()
                .Where(l => l.CityID == ID)
                .Select(l => new { Value = l.ID, Text = l.Name });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult FetchSubLocalities(int ID)
        {
            //int localityID = Repository.FetchLocalities().Where(l => l.ID == ID).Select(l => l.LocalityID).First();
            var data = Repository.FetchSubLocalities()
                .Where(l => l.LocalityID == ID)
                .Select(l => new { Value = l.ID, Text = l.Name });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void ConfigureViewModel(ViewModel model)
        {
            List<City> cities = Repository.FetchCities();
            model.CityList = new SelectList(cities, "ID", "Name");
            if (model.SelectedCity.HasValue)
            {
                IEnumerable<Locality> localities = Repository.FetchLocalities().Where(l => l.CityID == model.SelectedCity.Value);
                model.LocalityList = new SelectList(localities, "ID", "Name");
            }
            else
            {
                model.LocalityList = new SelectList(Enumerable.Empty<SelectListItem>());
            }
            if (model.SelectedLocality.HasValue)
            {
                IEnumerable<SubLocality> subLocalities = Repository.FetchSubLocalities().Where(l => l.LocalityID == model.SelectedLocality.Value);
            }
            else
            {
                model.SubLocalityList = new SelectList(Enumerable.Empty<SelectListItem>());
            }
        }
    }
}