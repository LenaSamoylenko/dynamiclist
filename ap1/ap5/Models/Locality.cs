﻿namespace ap5.Models
{
    public class Locality
    {
        public int ID { get; set; }
        public int CityID { get; set; }
        public string Name { get; set; }
    }
}
